# -*- coding: utf-8 -*-
"""
@file HW0x01.py
"""

def getChange(price, payment):
    payval = ((2000*payment[7]) + (1000*payment[6]) + (500*payment[5]) +
    (100*payment[4]) + (25*payment[3]) + (10*payment[2]) +
    (5*payment[1]) + (payment[0]))
    #print(str(payval))
    if (payval-price) < 0:
        change = None
        return change
    elif (payval-price) >= 0:
        twenties = 0
        tens = 0
        fives = 0
        ones = 0
        quarters = 0
        dimes = 0
        nickels = 0
        pennies = 0
        changeval = payval - price
        #print(str(changeval))
        while changeval >= 2000:
            twenties += 1
            changeval -= 2000
        #print(str(twenties))
        while changeval >= 1000:
            tens += 1
            changeval -= 1000
        #print(str(tens))
        while changeval >= 500:
                fives += 1
                changeval -= 500
        #print(str(fives))
        while changeval >= 100:
            ones += 1
            changeval -= 100
        #print(str(ones))
        while changeval >= 25:
            quarters += 1
            changeval -= 25
        #print(str(quarters))
        while changeval >= 10:
            dimes += 1
            changeval -= 10
        #print(str(dimes))
        while changeval >= 5:
            nickels += 1
            changeval -= 5
        #print(str(nickels))
        while changeval >= 1:
            pennies += 1
            changeval -= 1
        #print(str(pennies))
        change = (pennies, nickels, dimes, quarters, ones, fives, tens, twenties)
        return change



if __name__ == "__main__":
    mypay = (0, 0, 0, 0, 0, 0, 1, 0)
    change = getChange(525, mypay)
    print(str(change))